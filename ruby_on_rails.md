# Ruby on rails

## Characteristics

* Very useful and powerful for a _pure_ CRUD application
* Probably not the greatest candidate for an application with complex business logic

## Startup

* Install the latest Ruby version
* Run `gem install rails`

## Generate an API project base

* `rails new <your project name> --api --skip-sprockets --database=<your database>`

## Generate Rails components

* Model:
    * `rails generate model <model name>`
    * A DB migration script will be generated. Add the model's fields to this file.

* Controller:
    * `rails generate controller <controller name>`
    * Controller class name and filename must be an exact match

## Routing

* Routing rules are located in _confing/routes.rb_
* To bind all defined routes for a controller:
    * `resources :<controller_name>`
* Default routing can be overriden. Check Rails documentation.

## Secret and application variables management

* Secrets:
    * Values are located in _config/secrets.yml_
    * There are environment-specific values (dev, test, prod)
    * Leverage environment variables for PROD secrets. Do not store them in the code!
    * Access secret in the app: `Rails.application.secrets.<your_secret_variable_name>`
    * Set a value in _secrets.yml_:

```yaml
development:
    my_var: 1234

test:
    my_var: 4321

production:
    my_var: <%= ENV["MY_VAR_AS_ENVIRONMENT_VARIABLE"] %>
```

* Application variables:
    * Values are located in _config/environments/xyz.rb_
    * There is a file per environment (dev, test, prod)
    * Access a value in the app: `Rails.application.config.<your_variable_name>`
    * Set a value:

```ruby
Rails.application.configure do
    config.my_var = 1234
end
```

## Testing

* Using rspec as a test framework works really well for all level of test
* Rspec tests are located in the _rspec/_ folder
* All test files must end with `_spec.rb` to be detected by rspec
* To initialize rspec components: `rails generate rspec:install`
* To run the tests: `bundle exec rspec --format documentation`

## Running the app

* To run the app: `rails server [--port=xxxx] [--binding='0.0.0.0'] [--environment=production]`

## Dockerizing

* Here is a working sample Dockerfile for a Rails app with a PostgreSQL database

```Dockerfile
FROM ruby:2.3.3

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

COPY Gemfile /opt/<project_name>/Gemfile
COPY Gemfile.lock /opt/<project_name>/Gemfile.lock
WORKDIR /opt/<project_name>
RUN bundle install

ENV RAILS_ENV production
COPY . /opt/<project_name>

# rails server -p $PORT -b '0.0.0.0' -e production
CMD [ "./scripts/start_server.sh" ]
```
